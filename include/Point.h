#ifndef POINT_H
#define POINT_H

#include<iostream>
using namespace std;


class Point
{
    public:
        //constructor declaretion with params
        //we can init the params with a default values
        Point(int x=0, int y=0);
        virtual ~Point();

        //constructor copy
        Point(const Point &);

        //operator equal
        Point & operator=(const Point &right);

        //setters and getters
        void SetX(int);
        void SetY(int);
        int GetX();
        int GetY();

        //arimetics operators
        void operator+=(const Point &);
        void operator-=(const Point &);
        void operator/=(const Point &);
        void operator*=(const Point &);

        Point operator+(const Point &);
        Point operator-(const Point &);
        Point operator/(const Point &);
        Point operator*(const Point &);


        //declaration print method
        void print();
    protected:

    private:
        //point vars
        int x, y;
};

#endif // POINT_H
