#include "Point.h"

//constructor implementation with params
Point::Point(int x, int y)
{
    //this using a operator -> as pointer c++
    this->x = x;

    //this is the form to convert a pointer in instance
    (*this).y = y;

    //c++ using a pointers to take the control of the memory, (this) is a memory id.
}

//constructor copy impplementation
// other is a reference param of Point
Point::Point(const Point &other)
{
    x = other.x;
    y = other.y;

}

Point::~Point()
{
    //dtor
}

//point print method implementation
void Point::print()
{
    //print a point vars
    cout<<"x: "<<x<<endl;
    cout<<"y: "<<y<<endl;
}


// setters and getters implementations
void Point::SetX(int X)
{
    x=X;
}

void Point::SetY(int Y)
{
    y=Y;
}

int Point::GetX()
{
    return x;
}

int Point::GetY()
{
    return y;
}

//operators:
//copy
Point & Point::operator=(const Point &right)
{
    x = right.x;
    y = right.y;

    //we have to return a istance of this
    return *this;
}
//arimetics

void Point::operator+=(const Point &right)
{
    x += right.x;
    y += right.y;
}

void Point::operator-=(const Point &right)
{
    x -= right.x;
    y -= right.y;
}

void Point::operator*=(const Point &right)
{
    x *= right.x;
    y *= right.y;
}

void Point::operator/=(const Point &right)
{
    x /= right.x;
    y /= right.y;
}

Point Point::operator+(const Point &right)
{
    Point result(*this);
    result += right;
    return result;
}

Point Point::operator-(const Point &right)
{
    Point result(*this);
    result -= right;
    return result;
}

Point Point::operator*(const Point &right)
{
    Point result(*this);
    result *= right;
    return result;
}


Point Point::operator/(const Point &right)
{
    Point result(*this);
    result /= right;
    return result;
}



int main()
{
    //using the constructor
    Point a(2,3) , b;

    //we can use a operator = :
    Point c;
    //using the point print method

    b = c = a;

    c = a + b;
    a.print();
    b.print();
    c.print();

    return 0;
}
